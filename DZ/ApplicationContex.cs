﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ
{
    internal class ApplicationContex : DbContext  
    {

        public DbSet<Employee> employee { get; set;}
        public DbSet<Post> post { get; set;}
        public DbSet<cars> cars { get; set;}



        public ApplicationContex() 
        {
            Database.EnsureCreated();
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Username=postgres;Password=222;Database=ebay");
            base.OnConfiguring(optionsBuilder);
        }
    }


}
