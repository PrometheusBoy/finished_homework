﻿namespace DZ
{
    internal class Program
    {
        static void Main(string[] args)
        {
            AllDataBaseContext();
        }

        internal static void AllDataBaseContext() 
        {
            using (ApplicationContex context = new ApplicationContex())
            {
                InfoTable(context);
                //AddInformation(context);

            }
        }


        internal static void InfoTable (ApplicationContex context) 
        {
            var emp = context.employee.ToList();
            var car = context.cars.ToList();
            var post = context.post.ToList();

            Console.WriteLine("\tИнформация о сотрудниках:");
            foreach (var item in emp) 
            {
                Console.WriteLine("Сотрудник {0} {1} Возраст: {2} email: {3} Т/C: {4} Должность: {5}", item.first_name, item.last_name, item.age, item.email, item._cars.model, item._post.post);
            }

            Console.WriteLine("\n\tСписок Т/C сотрудников:");
            foreach (var item in car) 
            {
                Console.WriteLine(item.id + " : " + item.model + " : " + item.colour);
            }
            Console.WriteLine();
            Console.WriteLine("\n\tСписок должностей в компании и З/П:");
            foreach (var item in post)
            {
                Console.WriteLine(item.id + " : " + item.post + " : " + item.salary + "$");
            }
        }

    }
}