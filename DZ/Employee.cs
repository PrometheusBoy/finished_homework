﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ
{
    internal class Employee
    {
        public long id { get; set; }
        public string first_name { get; set; }

        public string last_name { get; set;}

        public string age { get; set;}

        public string email { get; set;}

        [ForeignKey("_cars")]
        public int cars_id { get; set; }

        public cars _cars { get; set; }
        
        [ForeignKey("_post")]
        public int post_id { get; set; }

        public Post _post { get; set; }
    }
}
