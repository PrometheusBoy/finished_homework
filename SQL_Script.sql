CREATE DATABASE ebay;

CREATE TABLE Employee
(
	Id SERIAL PRIMARY KEY,
	first_name VARCHAR (30) NOT NULL,
	last_name VARCHAR (30) NOT NULL,
	AGE VARCHAR (3) NOT NULL,
	email VARCHAR (70)
);
CREATE TABLE Cars
(
	Id SERIAL PRIMARY KEY,
	model VARCHAR (20) NOT NULL
);
CREATE TABLE Post
(	
	Id SERIAL PRIMARY KEY,
	post VARCHAR (20) NOT NULL,
	salary VARCHAR (30) NOT NULL
);

ALTER TABLE employee ADD cars_id BIGINT REFERENCES cars (id);
ALTER TABLE employee ADD post_id BIGINT REFERENCES post (id);

INSERT INTO post (post, salary) VALUES  ('Technical Director', '20000');
INSERT INTO post (post, salary) VALUES  ('Team-leader', '6000');
INSERT INTO post (post, salary) VALUES  ('FrontEnf-developer', '3000');
INSERT INTO post (post, salary) VALUES  ('BackEnd-developer', '4000');
INSERT INTO post (post, salary) VALUES  ('Intern', '1000');

INSERT INTO cars (model, colour) VALUES  ('BMW', 'RED');
INSERT INTO cars (model, colour) VALUES  ('Tesla', 'BLACK');
INSERT INTO cars (model, colour) VALUES  ('STELS Navigator-200', 'Blue');
INSERT INTO cars (model, colour) VALUES  ('Lamborghini', 'Orange');
INSERT INTO cars (model, colour) VALUES  ('ВАЗ Буханка', 'Dark Green');

INSERT INTO employee (first_name, last_name, age, email, cars_id, post_id) VALUES  ('John', 'Smith', '38', 'john_smithtechdirectori@gmail.com', 4, 1);
INSERT INTO employee (first_name, last_name, age, email, cars_id, post_id) VALUES  ('Jack', 'Black', '28', 'jackbblack_221gmail.com', 2, 2);
INSERT INTO employee (first_name, last_name, age, email, cars_id, post_id) VALUES  ('Stanley', 'London', '22', 'stan_onlyfront@gmail.com', 1, 3);
INSERT INTO employee (first_name, last_name, age, email, cars_id, post_id) VALUES  ('Олег', 'Иванов', '27', 'oleg_ivanov1337@gmail.com', 5, 4);
INSERT INTO employee (first_name, last_name, age, email, cars_id, post_id) VALUES  ('Oscar', 'White', '18', 'notwolterwhite@gmail.com', 3, 5);